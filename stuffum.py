class Stuffum:
    def __init__(self, name, description, pickable, picked, look_at, pick_up, hidden=False):
        self.name = name
        self.description = description
        self.pickable = pickable
        self.picked = picked
        self.look_at = look_at
        self.pick_up = pick_up
        self.hidden = hidden

def default_look_at_func(stuffum):
    if stuffum.picked:
        return "There is no " + stuffum.name + " to look at in this room. There used to be one here... You wonder who took it."
    return stuffum.description

def default_pickup_func(stuffum):
    if not stuffum.pickable:
        return "You can't pick up the " + stuffum.name + "."

    if not stuffum.picked:
        stuffum.picked = True
        return "You pick up the " + stuffum.name + "."

    return "You don't see a " + stuffum.name + " to pick up.  There used to be one here... You wonder who took it."

