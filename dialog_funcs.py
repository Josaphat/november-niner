import re
import time

azre = re.compile('[^a-zA-Z ]')

def normalize(s):
	return azre.sub('', s)

def norm_words(s):
	return [w.lower() for w in normalize(s).split(" ")]

def remove_prefixes(s, pre_lst):
	for pre in pre_lst:
		if len(s) >= len(pre) and s[:len(pre)] == pre:
			return s[len(pre):]

	return s

def remove_suffixes(s, suf_lst):
	for suf in suf_lst:
		if len(s) >= len(suf) and s[-len(suf):] == suf:
			return s[:-len(suf)]

	return s

def remove_prefix(s, pre):
	return remove_prefixes(s, [pre])

def has_prefix(s, pre):
    if len(s) < len(pre):
        return False

    return s[:len(pre)] == pre

def has_any_prefix(s, pres):
    for pre in pres:
        if has_prefix(s, pre):
            return True

    return False

def has_words_ordered(s, want_words):
	got_words = norm_words(s)
	want_lst = norm_words(want_words)
	seen = 0
	for word in got_words:
		if word == want_lst[seen]:
			seen += 1
		if seen == len(want_lst):
			return True

	return False

def has_words_unordered(s, want_words):
	want_set = set(norm_words(want_words))
	got_set = set(norm_words(got_words))

	return got_set.issuperset(want_set)

def has_any(s, words):
	want_set = set(norm_words(words))
	got_set = set(norm_words(s))

	return len(want_set.intersection(got_set)) > 0

def positive_response(s):
	return has_any(s, "yeah yes yep yup roger copy affirmative") \
		or s == "i think so"

def negative_response(s):
	return has_any(s, "won't don't doesn't no nope isn't negative didn't")

def show_dots(secs):
	dots = 0
	for _ in [x * 0.5 for x in range(0, int(secs*1.0/0.5))]:
		print('   ', end='\r')
		print("."*(dots+1), end='\r')
		dots = (dots + 1) % 3
		time.sleep(0.5)

def say(s):
	chars_per_sec = 1600*1.0/60 # HIgh-speed typists type around 400cpm; we're using 1600.

	show_dots(len(s)*1.0/chars_per_sec)

	print("CAM: " + s)
	print("")
