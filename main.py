#!/usr/bin/env python3
from pygame_textinput import pygame_textinput
from dialog import *
from dialog_funcs import azre
from dispatch_dialog import *
from stuffum import *
from text_pane import *
from first_cam_dialog import *
import re
import pygame
pygame.init()

# Create TextInput-object
textinput = pygame_textinput.TextInput()
textinput.set_text_color((0,255,0))
textinput.set_cursor_color((0,255,0))
def reset_textinput():
    global textinput
    textinput = pygame_textinput.TextInput()
    textinput.set_text_color((0,255,0))
    textinput.set_cursor_color((0,255,0))

def get_max_font_height(font):
    pass

txt_pan_width = 400

screen = pygame.display.set_mode((1000, 600))
pygame.display.set_caption('November Niner')
clock = pygame.time.Clock()
font = pygame.font.SysFont(pygame.font.get_default_font(), 24)
radio_font = pygame.font.Font("barcode.ttf", 24)

timer_events = []
total_time = 0

def wrap_msg(msg, font = font, pad = 0):
    ret = []
    words = msg.split(" ")
    buf = ""
    for word in words:
        if font.size(buf + " " + " " * pad + word)[0] > txt_pan_width:
            ret.append(str(buf))
            buf = ""
        buf += word
        buf += " "

    if buf != "":
        ret.append(buf)

    return ret

def lambda_suffix_factory(ch):
    return lambda: textpane.append_suffix(ch)

def lambda_append_factory(ch, color):
    return lambda: textpane.append(ch, color)

def queue_adv_msg(msg):
    last_event_time = pygame.time.get_ticks()
    for event in timer_events:
        if event[0] > last_event_time:
            last_event_time = event[0]

    timer_events.append((last_event_time, lambda: adventurelog.append(msg)))

def log_to_chat(thingtosay, player=False, delay_ms=0):
    # thingtosay = azre.sub('', thingtosay)
    wrapped = wrap_msg(thingtosay, font=radio_font, pad=0)
    if len(wrapped) > 1:
        _log_to_chat(wrapped[0], player, delay_ms)
        for msg in wrapped[1:]:
            _log_to_chat(msg, player, cont=True)
    else:
        for msg in wrapped:
            _log_to_chat(msg, player, delay_ms)

def _log_to_chat(thingtosay, player=False, delay_ms=0, cont=False):
    global textpane

    last_event_time = pygame.time.get_ticks()
    for event in timer_events:
        if event[0] > last_event_time:
            last_event_time = event[0]

    normal_delay = 0
    if not player:
        normal_delay = 2000
    if cont:
        normal_delay = 0

    time = last_event_time + normal_delay + delay_ms
    cursor = "> " if player else "< "
    if cont:
        cursor = "  "
    color = (255, 0, 0)
    if player:
        color = (0, 0, 255)
    timer_events.append((time, lambda_append_factory(cursor, color)))
    for char in thingtosay:
        time += 30
        timer_events.append((time, lambda_suffix_factory(char)))

def adv_log_queue(thingtosay):
    return log_to_adventure(thingtosay, queue=True)

# NOTE: Hoosaphat says that if we want to queue game state changes,
# we should just be janky and do it here as an optional func param
def log_to_adventure(thingtosay, player=False, queue=False):
    global adventurelog

    wrapped = wrap_msg(thingtosay)
    for msg in wrapped:
        if queue:
            queue_adv_msg(msg)
        else:
            adventurelog.append(msg)

def player_adv_log(thingtosay):
    return log_to_adventure(thingtosay, player=True, queue=False)

'''
Stuffum Delegates
'''

def radio_pickup_func(stuffum):
    if stuffum.picked:
        return "You already picked up the radio that was here."

    stuffum.picked = True
    global theworld
    # theworld.current_dialog = cam_dialog(log_to_chat, log_to_adventure, theworld)
    theworld.current_dialog = dispatch_dialog_1(log_to_chat, adv_log_queue, theworld)
    next(theworld.current_dialog)
    return "You pick up the radio. It looks like there's a transmission coming."


'''
ROOMS
'''

def default_room_go_delegate(s):
    cm = s.strip().lower()
    if "north" in cm:
        return theworld.move_north()
    if "south" in cm:
        return theworld.move_south()
    if "east" in cm:
        return theworld.move_east()
    if "west" in cm:
        return theworld.move_west()
    log_to_adventure("You don't know how to go there.")


class Room:
    def __init__(self, entry_flavor_lightson, stuff=[], going_delegate=default_room_go_delegate, known=True, locked=False, entry_flavor_lightsoff=None, north=None, south=None, east=None, west=None):
        self.entry_flavor_lightson = entry_flavor_lightson
        self.entry_flavor_lightsoff = entry_flavor_lightsoff
        self.stuff = stuff
        self.going_delegate = going_delegate
        self.known = known
        self.locked = locked
        self.north = north
        self.south = south
        self.east = east
        self.west = west
        # If you provide flavor text for when the lights are off, then
        # assume the room should start with the lights off
        if entry_flavor_lightsoff != None:
            self.lights = "off"
        else:
            self.lights = "on"

    def enter(self):
        if self.lights == "on":
            log_to_adventure(self.entry_flavor_lightson)
        else:
            log_to_adventure(self.entry_flavor_lightsoff)

    def add_north(self, room):
        self.north = room
        self.north.south = self
        return room
    def add_south(self, room):
        self.south = room
        self.south.north = self
        return room
    def add_east(self, room):
        self.east = room
        self.east.west = self
        return room
    def add_west(self, room):
        self.west = room
        self.west.east = self
        return room

    def look(self):
        log_to_adventure("You look around the room.")
        hasdoor = False
        if self.north != None and self.north.known:
            hasdoor = True
            log_to_adventure("- There's a door to the north")
        if self.south != None and self.south.known:
            hasdoor = True
            log_to_adventure("- There's a door to the south")
        if self.east != None and self.east.known:
            hasdoor = True
            log_to_adventure("- There's a door to the east")
        if self.west != None and self.west.known:
            hasdoor = True
            log_to_adventure("- There's a door to the west")

        # If there are no items which are both "not hidden" and "not picked", short-circuit. You don't see anything.
        if len([item for item in self.stuff if not item.picked and not item.hidden]) == 0:
            if hasdoor:
                log_to_adventure("You see nothing else.")
            else:
                log_to_adventure("You see nothing")
            return

        if hasdoor:
            log_to_adventure("You also see:")
        else:
            log_to_adventure("You see:")
        for stuffum in self.stuff:
            if not stuffum.picked and not stuffum.hidden:
                log_to_adventure("   - " + stuffum.name)

    def look_at(self, s):
        found = False
        for stuffum in self.stuff:
            if stuffum.name.lower() in s.strip().lower():
                log_to_adventure(stuffum.look_at(stuffum))
                found = True
        if not found:
            log_to_adventure(s.strip() + " is ordinary and unimportant.")

    def pick_up(self, s):
        found = False
        for stuffum in self.stuff:
            if stuffum.name.lower() in s.strip().lower():
                player_adv_log(stuffum.pick_up(stuffum))
                found = True
        if not found:
            log_to_adventure("There's no '" + s.strip() + "' to pick up.")

    def go(self, target):
        self.going_delegate(target)

class DispatchRoom(Room):
    def __init__(self, known=True, locked=False, north=None, south=None, east=None, west=None):
        radioDesc = "- It's your trusty DYNACorp radio. You realize that you should probably pick it up and get ready to go."
        badgeDesc = "- A Joshua Tree ID Badge, complete with your expressionless face. It won't do you much good this far from home."
        brochureDesc = "- Various brochures, all of them for places that have been shut down for almost 20 years."
        self.stuff = [Stuffum("radio", radioDesc, True, False, default_look_at_func, radio_pickup_func ),
                    Stuffum("badge", badgeDesc, False, False, default_look_at_func, default_pickup_func ),
                    Stuffum("brochure", brochureDesc, False, False, default_look_at_func, default_pickup_func)]
        self.known = True
        self.locked = False

    def enter(self):
        log_to_adventure("- You wake up again in the abandoned RV you've been lounging in for a few days now.")
        log_to_adventure("- It might not be yours, but it's nice to have some of the comforts of home in a new place.")
        log_to_adventure("- Besides, you're pretty confident nobody is going to care you're here.")
        log_to_adventure("- You're expecting somebody to radio you the specifics of your mission any minute now")
        log_to_adventure("(Try `/help` for a list of actions)")

    def look(self):
        log_to_adventure("- You look around the undersized RV, for what seems like the millionth time.")
        log_to_adventure("Amongst other things, you see:")
        if not self.stuff[0].picked:
            log_to_adventure("   - A DYNACorp two-way radio")
        log_to_adventure("   - An ID badge")
        log_to_adventure("   - An assortment of brochures")

    def look_at(self, s):
        found = False
        for stuffum in self.stuff:
            if stuffum.name.lower() in s.strip().lower():
                log_to_adventure(stuffum.look_at(stuffum))
                found = True
        if not found:
            log_to_adventure(s.strip() + " is ordinary and unimportant.")

    def pick_up(self, s):
        found = False
        for stuffum in self.stuff:
            if stuffum.name.lower() in s.strip().lower():
                player_adv_log(stuffum.pick_up(stuffum))
                found = True
        if not found:
            log_to_adventure("There's no '" + s.strip() + "' to pick up.")

    def go(self, target):
        dest_correct = not re.search("33\.[0-9]*,? -112\.[0-9]*", target) == None
        if dest_correct or "coordinate" in target:
            if not theworld.entrance_room.known:
                log_to_adventure("You scratch your head. 'What is " + target + "?' you ask yourself.")
                return False
            elif theworld.entrance_room.locked:
                log_to_adventure("You scratch your head. 'How do you get to " + target + "?' you ask yourself.")
                return False
            else:
                theworld.current_dialog = dispatch_dialog_2(log_to_chat, adv_log_queue, theworld)
                next(theworld.current_dialog)
                return theworld.move_to_room(theworld.entrance_room)
        else:
            log_to_adventure("You don't know how to go there.")
        return False

class BasementEntranceRoom(Room):
    def __init__(self, north=None, south=None, east=None, west=None):
        def second_glove_pickup_func(stuffum):
            # When you pick up the basement key, the basement door just becomes unlocked?
            if stuffum.picked:
                return "You don't see a right glove here. There used to be one here... You wonder who took it."
            stuffum.picked = True
            if theworld.basement_key_room.stuff[1].picked:
                return "You pick up the right glove. Now you have both gloves."
            else:
                return "You pick up the right glove. You doubt you'll ever find its partner."
        self.entry_flavor_lightson = ("The entrance to the basement. "
                                      "It's dank. There's a small shelf on the southern wall.")

        self.stuff = [
            Stuffum("shelf", "There's nothing on it besides dust and cobwebs.  You could swear the air is cooler when you're near it.", False, False, default_look_at_func, default_pickup_func),
            Stuffum("right glove", "There's a leather glove for a right hand on the floor under the shelf. It has what looks like a compass rose on it.", True, False, default_look_at_func, second_glove_pickup_func)
            ]
        self.known = True
        self.locked = False
        self.north = north
        self.south = south
        self.east = east
        self.west = west
        self.lights = "on"
        self.going_delegate = default_room_go_delegate

class EntranceRoom(Room):
    def __init__(self, north=None, south=None, east=None, west=None):
        holeDesc = "A non-descript hole in the wall. You have fond memories of struggling through it."
        self.stuff = [
                Stuffum("hole", holeDesc, False, False, default_look_at_func, default_pickup_func)
            ]
        self.north = north
        self.south = south
        self.east = east
        self.west = west
        self.known = False
        self.locked = False
        self.firstEnter = True      

    def enter(self):
        if self.firstEnter:
            log_to_adventure("- You've reached the entrance of a decrepit office building at the coordinates (33, -112).")
            log_to_adventure("- After circling the building you have come to the conclusion that the only entrance was a hole just large enough for you to squeeze through")
            log_to_adventure("- You climb through the hole, and into the building, and arrive to a frantic message on your radio.")
        else:
            log_to_adventure("- You return to the room you entered from, still empty, yet full of light from the entrance hole.")

    def go(self, target):
        if "north" in target.lower():
            return theworld.move_north()
        elif "south" in target.lower():
            log_to_adventure("- You briefly consider leaving, but you haven't retrieved the battery yet.")
            log_to_adventure("- Best get back to it.")
            return False
        elif "east" in target.lower():
            return theworld.move_east()
        elif "west" in target.lower():
            return theworld.move_west()
        return False

class LobbyRoom1(Room):
    def __init__(self, north=None, south=None, east=None, west=None):
        doorDesc = "A well built pair of sliding doors, held shut by the building's security system."
        deskDesc = "A receptionists desk faces the main doors to the building. It is coated in a layer of dust thick enough to make its former attendant roll in their grave."
        self.stuff = [
                Stuffum("doors", doorDesc, False, False, default_look_at_func, default_pickup_func),
                Stuffum("desk", deskDesc, False, False, default_look_at_func, default_pickup_func)
            ]
        self.north = north
        self.south = south
        self.east = east
        self.west = west
        self.known = True
        self.locked = False

    def enter(self):
        log_to_adventure("- Moving north from the empty entrance `hall`, you encounter another")
        log_to_adventure("- You appear to be in the intended lobbyway of the building.")
        log_to_adventure("- It has a pleasant mundanity to it. You wonder what it would be like to have a relaxing workday.")

    def go(self, target):
        cm = target.strip().lower()
        if "north" in cm:
            return theworld.move_north()
        if "south" in cm:
            return theworld.move_south()
        if "east" in cm:
            return theworld.move_east()
        if "west" in cm:
            return theworld.move_west()
        log_to_adventure("You don't know how to go there.")

class LobbyRoom2(Room):
    def __init__(self, north=None, south=None, east=None, west=None):
        doorDesc = "The main doors are opened. Along with the breeze, you feel a sense of relief that you won't be clambering through any more holes in the wall."
        deskDesc = "You pause to give a brief farewell to the receptionists desk. To your surprise, you see `DUSTIN NORRIS` scrawled in the dust."
        self.stuff = [
                Stuffum("doors", doorDesc, False, False, default_look_at_func, default_pickup_func),
                Stuffum("desk", deskDesc, False, False, default_look_at_func, default_pickup_func)
            ]
        self.north = north
        self.south = south
        self.east = east
        self.west = west
        self.known = True
        self.locked = False

    def enter(self):
        log_to_adventure("- You are back in the lobby of the building.")
        log_to_adventure("- Without any power, the main doors to the building have parted, and a cool breeze permeates the room.")

    def go(self, target):
        if "north" in target.lower():
            log_to_adventure("- You could go back into the northern part of the building, but Joshua Tree has been waiting for their battery long enough already.")
            return False
        elif "south" in target.lower():
            log_to_adventure("- You wistfully look through the doorway to the south. Light filters in through the gap in the wall.")
            log_to_adventure("- As much as you would love to crawl through that hole again, you think it best to walk through the main entrance")
            return False
        elif "east" in target.lower():
            return theworld.move_east()
        elif "west" in target.lower():
            return theworld.move_west()
        return False

class World:
    def __init__(self):
        self.next_dialog = None
        self.current_dialog = None
        self.can_chat = False

        # Create the dispatch room
        self.dispatch_room = DispatchRoom()

        # Create the entrance room
        self.entrance_room = EntranceRoom()

        # Create Room A1 - Lobby1
        self.a1 = LobbyRoom1()
        self.a1 = self.entrance_room.add_north(self.a1)

        # Room C1 (alternate A1)
        self.c1 = LobbyRoom2()
        self.outside = self.c1.add_east(Room("You win!"))

        # Room A2
        a2 = self.a1.add_north(Room("You enter the room. There seems to be nothing of interest in here.",
                               [],
                               default_room_go_delegate))
        # Basement Key Room
        def basement_key_pickup_func(stuffum):
            # When you pick up the basement key, the basement door just becomes unlocked?
            if stuffum.picked:
                return "You don't see a key here. There used to be one here... You wonder who took it."
            stuffum.picked = True
            return "You pick up the key.  Whatever door it's supposed to unlock, it will automatically happen."
        self.basement_key_room = a2.add_west(Room("It's an old storage room. Shelves line the walls. There's plenty of dust on them but not much else.",
                                                  [
                                                      Stuffum("key", "A key that looks like it can open a padlock", True, False, default_look_at_func, basement_key_pickup_func, hidden=True),
                                                      Stuffum("left glove", "There's a leather glove for a left hand on the floor under the shelf. It has what looks like a compass rose on it.", True, False, default_look_at_func, default_pickup_func, hidden=True)
                                                  ],
                                                  default_room_go_delegate,
                                                  entry_flavor_lightsoff="You enter a room that looks like it was used for storage, but it's dark in here."))

        # Basement Access Room
        def basement_access_go_delegate(s):
            if "basement" in s.lower() or "stairs" in s.lower():
                if theworld.basement_access_room.locked:
                    log_to_adventure("The door is locked")
                    return False
                log_to_adventure("You go to the basement")
                return theworld.move_to_room(theworld.basement_entrance)
            if "south" in s.lower():
                return theworld.move_south()
            if "north" in s.lower():
                return theworld.move_north()
            if "east" in s.lower():
                return theworld.move_east()
            if "west" in s.lower():
                return theworld.move_west()
            return False
        def breaker_panel_look_at_func(s):
            for stuffum in self.basement_key_room.stuff:
                stuffum.hidden = False
                self.basement_key_room.lights = "on"
            return "You instinctively toggle the breaker and hear a slight hum from somewhere down the hallways behind you. Presumably some lights came on in a different room."


        self.basement_access_room = a2.add_north(Room("This room has a hatch to the basement. There's a padlock preventing the hatch from opening. There's a panel on the wall.",
                                                      [
                                                          Stuffum("panel",
                                                                  "A circuit breaker panel.  It looks like one of the breakers was tripped; the label is faded beyond recognition. ",
                                                                  False,
                                                                  False,
                                                                  breaker_panel_look_at_func,
                                                                  default_pickup_func,
                                                                  hidden=False
                                                          )
                                                      ],
                                                      basement_access_go_delegate))

        self.basement_entrance = BasementEntranceRoom()

        def battery_pickup_func(stuffum):
            if not stuffum.pickable:
                return "You can't pick up the " + stuffum.name + "."
            if not stuffum.picked:
                stuffum.picked = True

                self.current_dialog = cam_dialog(log_to_chat, adv_log_queue, theworld)
                next(self.current_dialog)


                return "You pick up the " + stuffum.name + "."
            return "You don't see a " + stuffum.name + " to pick up.  There used to be one here... You wonder who took it."

        self.b2 = self.basement_entrance.add_east(Room("The room is filled with machines with blinking lights and meters on them.", [
            Stuffum("battery",
                    "A high capacity battery.  Engineered to provide power for months.",
                    True,
                    False,
                    default_look_at_func,
                    battery_pickup_func,
                    hidden=False)
        ]
        ))

        def hidden_basement_room_go_delegate(s):
            l = s.lower().strip()
            if "exit" in l or "HVAC" in l or "duct" in l:
                return theworld.move_to_room(self.c1)
            else:
                log_to_adventure("You don't know how to go to " + s.strip())
                return False
        self.b1 = self.basement_entrance.add_south(Room("You find yourself in a hidden room. There's a duct that looks like you could crawl through it.",
                                                        [Stuffum("HVAC duct",
                                                                 "A pathway that looks like it could lead out.",
                                                                 False,
                                                                 False,
                                                                 default_look_at_func,
                                                                 default_pickup_func,
                                                                 hidden=False
                                                        )],
                                                        known=False,
                                                        going_delegate=hidden_basement_room_go_delegate))
        # Start at the dispatch_room
        self.current_room = self.dispatch_room
        self.current_room.enter()

    def _move_dir(self, room):
        if room == None:
            log_to_adventure("There's nothing in that direction")
            return False
        if not room.known:
            log_to_adventure("There doesn't seem to be anything in that direction")
            return False
        if room.locked:
            log_to_adventure("The way is blocked")
            return False
        return self.move_to_room(room)

    def move_north(self):
        return self._move_dir(self.current_room.north)

    def move_south(self):
        return self._move_dir(self.current_room.south)

    def move_east(self):
        return self._move_dir(self.current_room.east)

    def move_west(self):
        return self._move_dir(self.current_room.west)

    def set_next_dialog(self, dialog):
        self.next_dialog = dialog

    def move_to_room(self, room):
        if not room.known:
            return False
        if room.locked:
            return False
        self.current_room = room
        self.current_room.enter()
        return True

    def set_can_chat(self):
        self.can_chat = True

    def yield_chat(self):
        last_event_time = pygame.time.get_ticks()
        for event in timer_events:
            if event[0] > last_event_time:
                last_event_time = event[0]

        timer_events.append((last_event_time, self.set_can_chat))

    def say(self, s):
        if not self.dispatch_room.stuff[0].picked:
            log_to_adventure("There's nobody here. You need to get a radio if you want to talk to somebody.")
            return

        if not self.can_chat:
            log_to_adventure("The indicator light will flash green when you can chat.", queue=False)
            return

        log_to_chat(s, player=True)
        if self.current_dialog == None:
            log_to_adventure("Nobody's listening on the other end...")
            return
        try:
            self.current_dialog.send(s)
            self.can_chat = False
        except StopIteration:
            if self.next_dialog is not None:
                self.current_dialog = self.next_dialog
                self.next_dialog = None
                next(self.current_dialog)
            else:
                self.current_dialog = None
            self.can_chat = False
            pass

    def process(self, s):
        lower = s.lower().strip()
        if lower.startswith("look at"):
            self.current_room.look_at(s[len("look at"):].strip())
        elif lower.startswith("check out"):
            self.current_room.look_at(s[len("check out"):].strip())
        elif "look" in lower:
            self.current_room.look()
        elif lower.startswith("pick up"):
            self.current_room.pick_up(s[len("pick up"):].strip())
        elif lower.startswith("take"):
            self.current_room.pick_up(s[len("take"):].strip())
        elif lower.startswith("crawl"):
            self.current_room.go(s[len("crawl"):].strip())
        elif lower.startswith("go to"):
            self.current_room.go(s[len("go to"):])
        elif lower.startswith("go"):
            self.current_room.go(s[len("go"):].strip())
        else:
            return False
        return True

adventurelog = TextPane((50, 75), (txt_pan_width, 450), font)
textpane = TextPane((550, 150), (txt_pan_width, 300), font)
background = pygame.image.load('resources/LayoutBoundingBoxes5.png')
green_background = pygame.image.load('resources/LayoutBoundingBoxesGreen.png')
backgroundRect = background.get_rect()


theworld = World()
last_flash_tick = pygame.time.get_ticks()
green = False

while True:
    screen.fill((225, 225, 225))
    if theworld.can_chat:
        if pygame.time.get_ticks() - last_flash_tick > 500:
            green = not green
            last_flash_tick = pygame.time.get_ticks()
        if green:
            screen.blit(background, backgroundRect)
        else:
            screen.blit(green_background, backgroundRect)
    else:
        screen.blit(background, backgroundRect)

    events = pygame.event.get()
    for event in events:
        if event.type == pygame.QUIT:
            exit()

    new_timer_events = []
    for event in timer_events:
        if event[0] <= pygame.time.get_ticks():
            event[1]()
        else:
            new_timer_events.append(event)
    timer_events = new_timer_events

    # Feed it with events every frame
    linedone = textinput.update(events)
    if linedone:
        out_string = textinput.get_text()
        reset_textinput()

        if out_string.startswith("/"):
            splits = out_string.split(' ', maxsplit=1)
            command = splits[0].strip().lower()
            if command == "/quit":
                exit()
            elif command == "/help":
                log_to_adventure("In general, you can:")
                log_to_adventure(" - `look around`")
                log_to_adventure(" - `look at` an object")
                log_to_adventure(" - `/say` things into the communicator")
                log_to_adventure(" - `pick up` an object")
            elif command == "/win":
                log_to_adventure("Congratulations! You've transmitted the corect command!")
                log_to_adventure("You've won!")
            elif command == "/talk" or command == "/say":
                if len(splits) == 1:
                    log_to_adventure("You can't /say nothing...")
                else:
                    theworld.say(splits[1].strip())
            elif command.startswith("/credit"):
                log_to_adventure("This game was created by: Lane Lawley, Josaphat Valdivia, and Matt Waite, with contributions from Janis and the help of GGJ2018-RIT")
            else:
                player_adv_log("Unknown Command: " + out_string)
        else:
            if len(out_string) != 0:
                player_adv_log("> " + out_string)
                processed = theworld.process(out_string)
                if not processed:
                    log_to_adventure("You attempt to " + out_string + " with a confused expression.")
                    log_to_adventure("Whatever that was, it didn't work.")

    textpane.draw(screen)
    adventurelog.draw(screen)

    # Blit its surface onto the screen
    screen.blit(textinput.get_surface(), (50, screen.get_height() - textinput.font_object.get_height() - 10))

    pygame.display.update()
    clock.tick(30)
