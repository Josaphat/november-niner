from dialog_funcs import *

# A dialog script is written as a Python coroutine.
# It yields out to receive input, which you send it with the "send" function.
# You can do whatever processing you want to the input, but several extracting convenience functions are provided.
# For now, we're just printing out the output, but this could be modified sent out to an output channel if need be.
def look_for_dyna_sign(say_func):
	dont_know_responses = [
		"Sorry, still not sure. Keep looking?",
		"That doesn't really help. Keep checking things out.",
		"Not sure about that. Let me know what else you see."
	]
	dkr = 0

	# These should also function as dont_know_responses, just in case the player is
	# pointing something out. But they should also remind them to stay on task in case the player
	# is fucking around.
	stay_on_task_responses = [
		"Keep looking around. Let me know if you see anything interesting.",
		"Check the room for identifying objects.",
	]
	sot = 0

	say_func("What sort of stuff do you see? Maybe there'll be something I recognize.")

	while True:
		rec = yield
		if has_words_ordered(rec, "sign") or has_words_ordered(rec, "dyna"):
			say_func("A blue sign? It says DYNA?")
			rec = yield
			if positive_response(rec):
				say_func("Great! I think I know where you are!")
				break
			else:
				say_func("Hmm...if it was a blue DYNA sign, I'd know where you are.")
		elif has_words_ordered(rec, "there's a") or has_words_ordered(rec, "there is a") or has_words_ordered(rec, "i see a"):
			say_func(dont_know_responses[dkr])
			dkr = (dkr + 1) % len(dont_know_responses)
		else:
			say_func(stay_on_task_responses[sot])
			sot = (sot + 1) % len(stay_on_task_responses)

	# At this point, they've found the sign.
	say_func("You're in the DYNACORP building on East Cherry.")
	say_func("It's been scabbed all to hell by now, but it has a standard door system.")
	say_func("I've seen it a million times. You'll be out in no time.")



if __name__ == '__main__':
	d1 = look_for_dyna_sign(say)
	next(d1) # Prime the dialog

	while True:
		try:
			d1.send(input("> "))
		except StopIteration:
			break

	print("Dialog finished")
