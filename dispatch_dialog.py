from dialog_funcs import *

def dispatch_dialog_1(chat_log, adventure_log, theworld):
    chat_log("Dispatch to November Niner.")

    adventure_log("- It sounds urgent. Maybe you should say something")

    # It doesn't actually matter how you respond to that.
    theworld.yield_chat()
    rec = yield

    chat_log("November Niner, stand by for directives.")

    chat_log("Alright, sorry for the delay, November Niner.", delay_ms=6000)
    chat_log("Proceed to 33.448506, -112.072226 and return as fast as you can.")

    chat_log("That basement battery should keep comms up for another month at least.")

    chat_log("How copy, November Niner?")

    # This response doesn't matter either.
    # Only your actions decide whether you'll go on the mission.
    theworld.yield_chat()
    rec = yield

    adventure_log("- Another day, another set of coordinates. Time to hit the road")
    theworld.entrance_room.locked = False
    theworld.entrance_room.known = True

def dispatch_dialog_2(chat_log, adventure_log, theworld):
    chat_log("Dispat.h to No.ember N..er.")
    chat_log("C.me in, Nov...er Nine.")

    theworld.yield_chat()
    rec = yield

    chat_log("So..y Nov..b.r ...e., pl..se rep..t")
    chat_log("The co... ba..ery is fina... g.in. out")
    chat_log("Re..rn w... b.t..ry AS.P")
    chat_log("....")
    chat_log("....")

    adventure_log("- You don't think you'll be hearing from dispatch again until you get that battery home.")

