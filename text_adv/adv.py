NORTH = 0
EAST = 1
SOUTH = 2
WEST = 3

dir_strs = {NORTH: "north", EAST: "east", SOUTH: "south", WEST: "west"}

def opposite_dir(direction):
	return (direction+2)%4

class Room:
	def __init__(self, name):
		self.name = name
		self.adj_rooms = [None for _ in range(4)]

	def north_room(self):
		return self.adj_rooms[NORTH]

	def east_room(self):
		return self.adj_rooms[EAST]

	def west_room(self):
		return self.adj_rooms[WEST]

	def south_room(self):
		return self.adj_rooms[SOUTH]

	def add_room(self, to_add, direction, recip=False):
		target = self.adj_rooms[direction]
		if target is None:
			self.adj_rooms[direction] = to_add
			# reciprocate if we're not already reciprocating
			if to_add is not None and not recip:
				to_add.add_room(self, opposite_dir(direction), True)
		elif target.name != to_add.name:
			raise RuntimeError("cannot add room %s as %s room of room %s: already added room %s" % (to_add.name, dir_strs[direction], self.name, target.name))

class RoomItem:
	def __init__(self, name, descr, actions):
		self.name = name
		self.descr = descr
		self.actions = actions

def make_map(room_tups):
	rooms = dict()

	# One pass to create the relevant rooms
	for room_tup in room_tups:
		room_name = room_tup[0]
		room_items = room_tup[1]
		if room_name in rooms:
			raise RuntimeError("room %s defined twice" % room_name)
		rooms[room_name] = Room(room_name)
		rooms[room_name].name = room_name
		rooms[room_name].items = room_items

	# Another pass to hook them up
	for room_tup in room_tups:
		room_name = room_tup[0]

		room_items = room_tup[1]

		for v in (2, 3, 4, 5):
			if room_tup[v] is not None and room_tup[v] not in rooms:
				raise RuntimeError("room %s does not exist" % (room_tup[v]))

		north_room = rooms[room_tup[2]] if room_tup[2] is not None else None
		east_room = rooms[room_tup[3]] if room_tup[3] is not None else None
		south_room = rooms[room_tup[4]] if room_tup[4] is not None else None
		west_room = rooms[room_tup[5]] if room_tup[5] is not None else None

		room = rooms[room_name]

		room.add_room(north_room, NORTH)
		room.add_room(east_room, EAST)
		room.add_room(south_room, SOUTH)
		room.add_room(west_room, WEST)

		rooms[room_name]


if __name__ == '__main__':
	# Here's an example of defining rooms.
	# If the directional relationships are inconsistent, it will tell you.

	room_tups = [
		("Center Room", [], "North Room", "East Room", None, "West Room"),
		("North Room", [], None, None, "Center Room", None),
		("East Room", [], None, None, None, "Center Room"),
		("West Room", [], None, "Center Room", None, None),
	]

	room_map = make_map(room_tups)
