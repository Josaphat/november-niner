from choices import *
from dialog_funcs import *

def cam_dialog(chat_log, adventure_log, the_world):
    chat_log("Hey, is someone messing with the battery?")
    chat_log("The camp power just went out.")

    the_world.yield_chat()
    rec = yield

    chat_log("Who is this?")

    the_world.yield_chat()
    rec = yield

    if has_words_ordered(rec, "who are you"):
        chat_log("I'm Dustin.")

    chat_log("Are you the one who just knocked out our power?")

    the_world.yield_chat()
    rec = yield

    if positive_response(rec):
        choices["truthful_about_power"] = True
        chat_log("Why would you do that?")
        # The player is "justified" if they mention something about
        # their settlement, their transmitter, their mission, etc.
        the_world.yield_chat()
        rec = yield

        if has_any(rec, "transmitter settlement city  mission sent supposed"):
            chat_log("Look, I know power's hard to come by, but that was ours.")
        else:
            chat_log("You can't just go around messing with stuff like that.")
            chat_log("That battery was important to us.")

        # while loop to check for lobby?
    else:
        choices["truthful_about_power"] = False

    chat_log("Well, anyway, you probably put the building into lockdown.")
    chat_log("You won't be able to get out without overriding the security.")
    chat_log("Hang on a minute while I see if anyone here knows where the panels are.")

    chat_log("Alright, looks like we have a list of instructions from the last time this happened.", delay_ms=15000)
    chat_log("First, you're going to want to go to the lobby to shut off the alarm.")
    chat_log("There should be an alternative path back up behind the shelf.")
    the_world.b1.known = True
    chat_log("Let me know when you're in the lobby.")

    the_world.set_next_dialog(cam_wait_lobby(chat_log, adventure_log, the_world))

def cam_wait_lobby(chat_log, adventure_log, the_world):
    while True:
        the_world.yield_chat()
        rec = yield
        chat_log("You're in the lobby?")
        the_world.yield_chat()
        rec = yield
        if positive_response(rec):
            chat_log("Great. I don't care. The game is over.")
            break
        else:
            chat_log("Let me know when you're in the lobby. Look for an alternative path there behind the shelf.")
