class TextPane:
    def __init__(self, pos, dim, font, border_color = None):
        self.pos = pos
        self.font = font
        self.dim = dim
        self.height = self.dim[1] // font.get_height()
        self.start = 0
        self.end = self.height
        self.buf = [""] * self.height
        self.buf_color = [(0, 255, 0) for _ in range(self.height)]
        self.border_color = border_color
        self.visible = True

    def append(self, text, color = (0, 255, 0)):
        self.buf.append(text)
        self.buf_color.append(color)
        self.start += 1
        self.end += 1

    def append_suffix(self, character):
        self.buf[self.end-1] += character

    def draw(self, screen):
        i = 0
        for idx in range(self.start, self.end):
            text = self.buf[idx]
            color = self.buf_color[idx]
            rendered = self.font.render(text, True, color)
            screen.blit(rendered, (self.pos[0], self.pos[1] + i * self.font.get_height()))
            i += 1
        if self.border_color != None:
            pygame.draw.rect(screen, self.border_color, (self.pos, self.dim), 2)
