from dialog_funcs import *

what_prefixes = ["what is", "what are", "what was"]

def cam_dialog(chat_log, adventure_log):
    unused_response = False
    while True:
        if unused_response:
            unused_response = False
        else:
            rec = yield

        # START: where are you / where do you live / etc.
        if has_words_ordered(rec, "where you"):
            chat_log("Sorry, but I'm not really comfortable sharing that with someone I don't know.")
            chat_log("You could be a scabber.")
            chat_log("You understand, right?")
            rec = yield
            if positive_response(rec):
                chat_log("Thanks.")
                chat_log("Anyway, let's work on getting you out of there.")
            elif negative_response(rec):
                chat_log("Well, that's how it is.")
                chat_log("Anyway, let's work on getting you out of there.")
            else:
                unused_response = True
        # END: where are you

        # START: what is <x>
        if has_any_prefix(rec, what_prefixes):
            topic = normalize(remove_prefixes(rec, what_prefixes)).lower().strip()
            print("topic is: " + topic)
            if topic == "joshua tree":
                # TODO: if Cam knows you're with joshua tree, they should give a different answer
                chat_log("Joshua Tree is a huge settlement to the southeast of here.")
                chat_log("They're named after the old park they set up shop in.")
                chat_log("They've probably got a few thousand people now. Like a real city, pre-war.")
                chat_log("We compete a lot for supplies, but they're generally peaceful.")
            if has_words_ordered(topic, "war"):
                chat_log("Unless you're younger than you sound, I think you must be kidding.")
                chat_log("Don't kid around about the war, please.")
                chat_log("We all lost a lot.")
            if has_words_ordered(topic, "your group") or has_words_ordered(topic, "your city") or has_words_ordered(topic, "your settlement"):
                chat_log("I'm with a small group of people here in the city.")
                chat_log("We don't really call ourselves anything.")
                chat_log("I'd prefer not to reveal a whole lot more.")
                chat_log("I'm sure you understand.")
                rec = yield
                if positive_response(rec):
                    chat_log("Thanks.")
                elif negative_response(rec):
                    chat_log("Well, that's how it is.")
                else:
                    unused_response = True
            
            
        # END: what is <x>
